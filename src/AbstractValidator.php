<?php
namespace Sinta\LaravelValidator;

use Illuminate\Support\MessageBag;
use Sinta\LaravelValidator\Contracts\ValidatorInterface;
use Sinta\LaravelValidator\Exceptions\ValidatorException;


abstract class AbstractValidator implements ValidatorInterface
{
    protected $id = null;

    protected $validator;

    protected $data = array();

    protected $rules = array();


    protected $messages = array();


    protected $attributes = array();

    protected $errors = array();

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    public function with(array $data)
    {
        $this->data = $data;
        return $this;
    }

    public function errors()
    {
        return $this->errorsBag()->all();
    }

    public function errorsBag()
    {
        return $this->errors;
    }


    abstract public function passes($action = null);

    public function passesOrFail($action = null)
    {
        if (!$this->passes($action)) {
            throw new ValidatorException($this->errorsBag());
        }
        return true;
    }

    public function getRules($action = null)
    {
        $rules = $this->rules;
        if (isset($this->rules[$action])) {
            $rules = $this->rules[$action];
        }
        return $this->parserValidationRules($rules, $this->id);
    }


    public function setRules(array $rules)
    {
        $this->rules = $rules;
        return $this;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setMessages(array $messages)
    {
        $this->messages = $messages;
        return $this;
    }


    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }


    protected function parserValidationRules($rules, $id = null)
    {
        if (null === $id) {
            return $rules;
        }
        array_walk($rules, function (&$rules, $field) use ($id) {
            if (!is_array($rules)) {
                $rules = explode("|", $rules);
            }
            foreach ($rules as $ruleIdx => $rule) {
                // get name and parameters
                @list($name, $params) = array_pad(explode(":", $rule), 2, null);
                // only do someting for the unique rule
                if (strtolower($name) != "unique") {
                    continue; // continue in foreach loop, nothing left to do here
                }
                $p = array_map("trim", explode(",", $params));
                // set field name to rules key ($field) (laravel convention)
                if (!isset($p[1])) {
                    $p[1] = $field;
                }
                // set 3rd parameter to id given to getValidationRules()
                $p[2] = $id;
                $params = implode(",", $p);
                $rules[$ruleIdx] = $name.":".$params;
            }
        });
        return $rules;
    }

}