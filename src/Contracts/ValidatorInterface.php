<?php
namespace Sinta\LaravelValidator\Contracts;

use Illuminate\Contracts\Support\MessageBag;
use Sinta\LaravelValidator\Exceptions\ValidatorException;

interface ValidatorInterface
{
    const RULE_CREATE = 'create';
    const RULE_UPDATE = 'update';


    public function setId($id);


    public function with(array $input);


    public function passes($action = null);


    public function passesOrFail($action = null);


    public function errors();


    public function errorsBag();


    public function setRules(array $rules);


    public function getRules($action = null);

}