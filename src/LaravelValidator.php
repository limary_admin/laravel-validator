<?php
namespace Sinta\LaravelValidator;

use Illuminate\Contracts\Validation\Factory;

class LaravelValidator extends AbstractValidator
{
    protected $validator;


    public function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }


    /**
     * 是否通过验证
     *
     * @param null $action
     * @return bool
     */
    public function passes($action = null)
    {
        $rules      = $this->getRules($action);
        $messages   = $this->getMessages();
        $attributes = $this->getAttributes();
        $validator  = $this->validator->make($this->data, $rules, $messages, $attributes);

        if($validator->fails()){
            $this->errors = $validator->messages();
            return false;
        }
        return true;
    }
}